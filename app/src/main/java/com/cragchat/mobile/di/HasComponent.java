package com.cragchat.mobile.di;

/**
 * Created by timde on 1/24/2018.
 */

public interface HasComponent<C> {
    C getComponent();
}
