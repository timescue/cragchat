package com.cragchat.mobile.ui.model;

/**
 * Created by timde on 11/28/2017.
 */

public interface NewCommentVoteRequest {

    String getVote();

    String getCommentKey();

}
